numpy==1.16.4
pygame==1.9.6
Pillow==6.2.0
numba==0.44.1
torch==1.1.0
torchvision==0.3.0